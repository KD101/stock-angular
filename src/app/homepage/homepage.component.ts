import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";



@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {

  stockPortfolio: any = [];

  constructor(
    public restApi: RestApiService
  ) { }

  ngOnInit(): void {
    this.loadPortfolio()
  }

  loadPortfolio() {
    return this.restApi.getPortfolios().subscribe((data: {}) => {
        this.stockPortfolio = data;
    })
  }

  deletePortfolio(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteStockPortfolio(id).subscribe(data => {
        this.loadPortfolio
      })
    }
  }



}




