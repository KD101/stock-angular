import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stock } from '../shared/stock';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { StockPortfolio } from './stock-portfolio';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = 'http://spring-hackathon-spring-hackathon.emeadocker12.conygre.com/api/v1/stock/';
  apiURLPortfolio = 'http://spring-hackathon-spring-hackathon.emeadocker12.conygre.com/api/v1/portfolio/'
  apiURLHistoricalPrices = "https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2"
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // HttpClient API get() method
  getStocks(): Observable<Stock> {
    return this.http.get<Stock>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method
  getPriceData(stockTicker: string): Observable<any> {
    return this.http.get<any>(this.apiURLHistoricalPrices + "?ticker=" + stockTicker + "&num_days=" + 10)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }




  // HttpClient API get() method
  getStock(id:any): Observable<Stock> {
    return this.http.get<Stock>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getStockByTicker(stockTicker: string): Observable<any> {
    return this.http.get<any>(this.apiURL + '?stockTicker=' +stockTicker)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method
  createStock(stock:Stock): Observable<Stock> {
    return this.http.post<Stock>(this.apiURL + '', JSON.stringify(stock), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*updateHoldings(stock:any): Observable<Stock> {
    return this.http.put<Stock>(this.apiURLPortfolio + 'update/holdings' + '', stock, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }*/

  // HttpClient API put() method
  updateStock(id:number, stock:Stock): Observable<Stock> {
    return this.http.put<Stock>(this.apiURL + '', JSON.stringify(stock), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method
  deleteStock(id:number){
    return this.http.delete<Stock>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  //
  // -------------------------Stock Portfolio Methods
  //

  //HTTP Client API Get Portfolio
  getPortfolios(): Observable<StockPortfolio> {
    return this.http.get<StockPortfolio>(this.apiURLPortfolio)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HTTP API Get Portfolio
  getPortfolio(id:any): Observable<StockPortfolio> {
    return this.http.get<StockPortfolio>(this.apiURLPortfolio + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }


  // HttpClient API post() method
  createPortfolioEntry(portfolio:StockPortfolio): Observable<StockPortfolio> {
    return this.http.post<StockPortfolio>(this.apiURLPortfolio + '', JSON.stringify(portfolio), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API put() method
  updateStockPortfolio(id:number, portfolio:StockPortfolio): Observable<StockPortfolio> {
    return this.http.put<StockPortfolio>(this.apiURLPortfolio + '', JSON.stringify(portfolio), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method
  deleteStockPortfolio(id:number){
    return this.http.delete<StockPortfolio>(this.apiURLPortfolio + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }



  //
  // ----------------------------Error Handling
  //


  // Error handling
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
