import { NumberValueAccessor } from "@angular/forms";
import { Stock } from "./stock";

export class StockPortfolio {

constructor(){
  this.portfolioid=0;
    this.stockTicker='';
    this.holdings=0;
    this.avgPrice=0;
    this.value=0;
}
portfolioid: number;
stockTicker: string;
holdings: number;
avgPrice: number;
value: number;
}
