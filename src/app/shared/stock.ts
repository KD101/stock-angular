import { DecimalPipe } from "@angular/common";

export class Stock {
    constructor(){
        this.createdTimeStamp=''; 
        this.id=0;
        this.stockTicker='';
        this.buyOrSell='';
        this.price=0.0;
        this.volume=0;
        this.statusCode=0;
    }
    createdTimeStamp: string; 
    id: number;
    stockTicker: string;
    buyOrSell: string;
    price: number;  
    volume: number;
    statusCode: number;
}
