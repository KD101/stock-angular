import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockEditComponent } from './stock-edit/stock-edit.component';
import { StockListComponent } from './stock-list/stock-list.component';
import {HomepageComponent} from './homepage/homepage.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'homepage' },
  { path: 'stock-create', component: StockCreateComponent },
  { path: 'stock-list', component: StockListComponent },
  { path: 'stock-edit/:id', component: StockEditComponent },
  {path: 'homepage', component: HomepageComponent},
  { path: '**', component: StockListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
