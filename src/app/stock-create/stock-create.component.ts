import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-stock-create',
  templateUrl: './stock-create.component.html',
  styleUrls: ['./stock-create.component.css']
})
export class StockCreateComponent implements OnInit {

  @Input() stockDetails = {createdTimeStamp: '', id:0, stockTicker: '', buyOrSell:'', price:0, volume:0, statusCode:0}

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit() { }

  addStock() {
    this.restApi.createStock(this.stockDetails).subscribe((data: {}) => {
      //this.restApi.updateHoldings(data)
      //console.log(data)
      this.router.navigate(['/stock-list'])
    })
  }

}
