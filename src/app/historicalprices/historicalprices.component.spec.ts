import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalpricesComponent } from './historicalprices.component';

describe('HistoricalpricesComponent', () => {
  let component: HistoricalpricesComponent;
  let fixture: ComponentFixture<HistoricalpricesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricalpricesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalpricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
